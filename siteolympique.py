#!/bin/python
import requests, json


def get_olympic_sites_in_paris():
    url = 'https://data.culturecommunication.gouv.fr/api/explore/v2.1/catalog/datasets/jo2024_communes-labellisees/records?limit=20'
    response = requests.get(url)

    if response.status_code == 200:
        datas = json.loads(response.text)
 #       print(datas['results'])
        #print("Liste des sites olympiques dans Paris pour les Jeux Olympiques de Paris 2024 :")
        villes = []
        for element in datas['results']:
            villes.append(element['libelle_geographique'])

        print('Voici les villes sites olympique:')
        for element in villes:
             print(element)

if __name__ == "__main__":
     get_olympic_sites_in_paris()

